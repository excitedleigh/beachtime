FROM python:3.6.2-alpine
COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
COPY script.py /opt/beachtime.py
CMD python /opt/beachtime.py
