# beachtime

A script that sends you a SMS when the weather is nice, suggesting you go to the beach.

Beachtime uses the [Dark Sky API] to get weather, [Twilio] to send SMS messages, and is designed to be run on [Hyper Cron].

[Dark Sky API]: https://darksky.net/dev/
[Twilio]: https://www.twilio.com/
[Hyper Cron]: https://docs.hyper.sh/Feature/container/cron.html

## Usage

1. Sign up for Hyper, Dark Sky and Twilio (linked above). Collect your API keys for the latter two.
    - For **Dark Sky**, you won't need to provide a credit card for the free tier, and this script will only produce one request each time it is run (you get 1000 daily for free).
    - For **Hyper**, you _will_ need to provide a credit card before you can run anything, but you'll only be billed while the script is running, so it should only cost a fraction of a cent per month.
    - For **Twilio**, you _will_ need to provide a credit card, and you'll be charged for two messages to each recipient each time the script decides to send them ($0.057 per recipient in Australia at the time of writing).
2. [Install the Hyper command line client](https://docs.hyper.sh/GettingStarted/install.html), if you haven't already.
3. Download the `security-group.yaml` file, and run `hyper sg create -f security-group.yaml beachtime-sg`.
3. Run the command below. Note that you will have to replace the `--hour` and `--minute` arguments for when you want the script to run in UTC time, and the example settings values with real ones.

```
hyper cron create --hour=05 --minute=30 --name beachtime \
    --size=s1 --sg=beachtime-sg \
    --env DARK_SKY_SECRET=7029845289bc8b07bdf127401f529636 \
    --env TWILIO_SID=ded9544d --env TWILIO_TOKEN=1e3e51dc0dd97158 \
    --env BEACH_LOCATION=-34.8382,138.4802 \
    --env START_TIME=1800 --env END_TIME=2100 \
    --env MIN_TEMP=25 --env MAX_TEMP=38 \
    --env MAX_PRECIP=10 --env MAX_HUMIDITY=60 \
    --env RECIPIENTS=61491570156,61491570110 \
    adambrenecki/beachtime
```

## Settings

- `DARK_SKY_SECRET`: Secret key for the Dark Sky API.
- `TWILIO_SID` and `TWILIO_TOKEN`: The SID and secret token for the Twilio API.
- `BEACH_LOCATION`: The beach you want to fetch weather for, in `lat,lon` format.
- `START_TIME`: The time you expect to get to the beach, in 24-hour HHMM time. The script takes the weather at this time to decide whether or not to send the SMS.
- `END_TIME`: The time you'd normally leave the beach. Weather data at this time is included in the SMS, but isn't used to make any decisions by the script.
- `MIN_TEMP` and `MAX_TEMP`: Only send a SMS when the temperature is in this range.
- `MAX_PRECIP`: Only send a SMS when the precipitation **probability** is below this percentage.
- `MAX_HUMIDITY`: Only send a SMS when the humidity is below this percentage.
- `RECIPIENTS`: Comma-separated list of numbers to send SMSes to, in international format.
