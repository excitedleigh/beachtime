from os import environ as env
import requests
from pytz import timezone
from datetime import date, time, datetime, timedelta
from urllib.parse import quote
from twilio.rest import Client

twilio_client = Client(env['TWILIO_SID'], env['TWILIO_TOKEN'])


def get_datetime_in_future(tz, time_string):
    """Gets the next occurrence of `time_string` in `tz`.

    `time_string` should be a string in 24-hour HHMM format.
    """
    now = datetime.now(tz)
    hours, minutes = int(time_string[:2]), int(time_string[2:])
    value = tz.localize(datetime.combine(now.date(), time(hours, minutes)))
    if value < now:
        value += timedelta(days=1)
    return value


def abort(message, weather):
    print(message, "It will be", format_weather(weather)+".")
    exit(0)


def format_weather(weather):
    return ("{apparentTemperature:.1f} and {summary}, with "
            "{precipProbabilityPct:.0f}% chance of rain, {humidityPct:.0f}% "
            "humidity and {cloudCoverPct:.0f}% cloud cover").format(
                precipProbabilityPct=weather['precipProbability'] * 100,
                humidityPct=weather['humidity'] * 100,
                cloudCoverPct=weather['cloudCover'] * 100,
                **weather,
            )


if __name__ == "__main__":
    # Fetch weather data.
    weather_url = 'https://api.darksky.net/forecast/{}/{}?units=si'.format(
        env['DARK_SKY_SECRET'], env['BEACH_LOCATION']
    )
    weather = requests.get(weather_url).json()

    # Generate times
    tz = timezone(weather['timezone'])
    start_time = get_datetime_in_future(tz, env['START_TIME'])
    end_time = get_datetime_in_future(tz, env['END_TIME'])

    # find weather for times
    start_weather, end_weather = None, None
    for hour in weather['hourly']['data']:
        if hour['time'] == start_time.timestamp():
            start_weather = hour
        elif hour['time'] == end_time.timestamp():
            end_weather = hour
        if start_weather is not None and end_weather is not None:
            break
    else:
        raise ValueError("Forecast.io did not return data for both start and "
                         "end times")

    # Decide if it's good beach weather
    if start_weather['apparentTemperature'] < int(env['MIN_TEMP']):
        abort("Too cold for the beach, brrrrr!", start_weather)

    if start_weather['apparentTemperature'] > int(env['MAX_TEMP']):
        abort("Too hot for the beach! Oh God it burns!", start_weather)

    if start_weather['precipProbability'] * 100 > int(env['MAX_PRECIP']):
        abort("Too likely to rain! Oh no!", start_weather)

    if start_weather['humidity'] * 100 > int(env['MAX_HUMIDITY']):
        abort("Too humid! Ew!", start_weather)

    # Generate the message
    message = ("Beach time! "
               "At {start_time:%H:%M} it will be {start_weather}. "
               "By {end_time:%H:%M} it will be {end_weather}. ").format(
                   start_time=start_time,
                   start_weather=format_weather(start_weather),
                   end_time=end_time,
                   end_weather=format_weather(end_weather),
               )
    print(message)
    for recipient in env['RECIPIENTS'].split(','):
        twilio_client.messages.create(
            to=recipient,
            from_='BeachTime',
            body=message,
        )
